const helper = require('../util/helper');

exports.getUsers = (req, res, next) => {
    const provider = req.query.provider;
    const statusCode = req.query.statusCode;
    const balanceMin = req.query.balanceMin;
    const balanceMax = req.query.balanceMax;
    const currency = req.query.currency;

    var users =  helper.getUsersData(provider, statusCode, balanceMin, balanceMax, currency);

    res.status(201).json({
        message: 'Success.',
        users: users
    });
};







