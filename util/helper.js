const fs = require('fs');



exports.getUsersXData = () =>  {
    let DataProviderX = fs.readFileSync('DataProviderX.json');
    let DataX = JSON.parse(DataProviderX);
    var users = DataX.users;
    return users;
};

exports.getUsersYData = () =>  {
    let DataProviderY = fs.readFileSync('DataProviderY.json');
    let DataY = JSON.parse(DataProviderY);
    var users = DataY.users;
    return users;
};

exports.getAllUsers = (statusCode) => {
    var usersX = this.getUsersXData();
    var usersY = this.getUsersYData();
    var users = usersX.concat(usersY);
    return users;
};


exports.getUsersData = (provider, statusCode , balanceMin , balanceMax, currency) => {

    var usersX = this.getUsersXData();
    var usersY = this.getUsersYData();
    

    if(provider == 'DataProviderX'){
        var users = usersX;
    }else if (provider == 'DataProviderY'){
        var users = usersY;
    }else{
        var users = usersX.concat(usersY);
    }

    if (statusCode == 'authorised'){
        users = users.filter(s => s.statusCode == '1' || s.status == '100');
    }else if (statusCode ==  'decline'){
        users = users.filter(s => s.statusCode == '2' || s.status == '200');
    }else if (statusCode ==  'refunded'){
        users = users.filter(s => s.statusCode == '3' || s.status == '300');
    }

    if(balanceMin && balanceMax){
        users = users.filter(s => (s.parentAmount >= balanceMin && s.parentAmount <= balanceMax) || (s.balance >= balanceMin && s.balance <= balanceMax));
    }

    if(currency){
        users = users.filter(s => s.Currency == currency || s.currency == currency);
    }

    return users;
};
